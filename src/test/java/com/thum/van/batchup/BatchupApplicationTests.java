package com.thum.van.batchup;

import com.thum.van.batchup.config.BatchConfiguration;
import com.thum.van.batchup.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BatchupApplicationTests {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;

    @Autowired
    PersonRepository personRepository;

    @Test
    public void importPeoples() throws Exception{
        int initial = personRepository.findAll().size();
        log.info("=============== TEST - initial size = " + initial + "===============");
        jobLauncher.run(job, new JobParameters());
        int countOfNewPeople = 5;
        log.info("=============== TEST - total size = " + personRepository.findAll().size() + "===============");
        Assert.assertEquals(initial + countOfNewPeople, personRepository.findAll().size());
    }

    @Test
    public void importPeoplesWithErrors() throws Exception{
        int initial = personRepository.findAll().size();
        log.info("=============== TEST - initial size = " + initial + "===============");
        jobLauncher.run(job, new JobParameters());
        int countOfNewPeople = 4;
        log.info("=============== TEST - total size = " + personRepository.findAll().size() + "===============");
        Assert.assertEquals(initial + countOfNewPeople, personRepository.findAll().size());
    }
}
