package com.thum.van.batchup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchupApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchupApplication.class, args);
    }
}