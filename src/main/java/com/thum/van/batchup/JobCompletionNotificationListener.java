package com.thum.van.batchup;

import com.thum.van.batchup.domain.Person;
import com.thum.van.batchup.repository.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

    @Autowired
    PersonRepository personRepository;

    @Override
    public void afterJob(JobExecution jobExecution){
        if(jobExecution.getStatus() == BatchStatus.COMPLETED){
            log.info("----------JOB IS COMPLETED----------");
            personRepository.findAll().forEach(person -> log.info("Found <" + person + "> in the database."));
            log.info("SIZE = " + personRepository.findAll().size());
        }
    }
}