package com.thum.van.batchup;

import com.thum.van.batchup.domain.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

    @Override
    public Person process(Person person) throws Exception {
        final String name = person.getName().toUpperCase();
        final String surname = person.getSurname().toUpperCase();
        final String email = person.getEmail().toUpperCase();
        Person transformedPerson = new Person(name, surname, email);
        log.info("Transformed person -----" + transformedPerson);
        return transformedPerson;
    }
}
