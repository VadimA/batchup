package com.thum.van.batchup.repository;

import com.thum.van.batchup.domain.Person;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PersonRepository extends MongoRepository<Person, String> {
    public List<Person> findByName(String name);
    public List<Person> findBySurname(String surname);
}